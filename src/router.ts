import Vue from "vue";
import Router from "vue-router";
//layout
import BlankLayout from './layouts/BlankLayout.vue'
import BaseLayout from './layouts/BaseLayout.vue'


// views
import Home from "./views/Home.vue";
import Main from "./views/Main.vue";
import UserInfo from "./views/UserInfo.vue";
import Password from "./views/Password.vue";
import Todo from "./views/Todo.vue";
import History from "./views/History.vue";

// components
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";

// user
import UserList from "./views/user/UserList.vue";
import UserActive from "./views/user/UserActive.vue";

// product
import ProductAdd from "./views/product/ProductAdd.vue";
import ProductDown from "./views/product/ProductDown.vue";
import ProductList from "./views/product/ProductList.vue";




Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "登录",
      component: Login
    },
    {
      path: "/about",
      name: "about",

      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      path: "/main",
      name: "",
      component: BaseLayout,
      redirect: '/main/home',
      children:[
        {
          path: "home",
          name: "首页",
          component: Home
        },
        {
          path: "todo",
          name: "我的待办",
          component: Todo
        },
        {
          path: "history",
          name: "历史记录",
          component: History
        },
        {
          path: "userInfo",
          name: "我的资料",
          component: UserInfo
        },
        {
          path: "password",
          name: "修改密码",
          component: Password
        },
        {
          path:"user",
          name:"用户管理",
          component: BlankLayout,
          children:[
            {
              path: "list",
              name: "用户查询",
              component: UserList
            },
            {
              path: "active",
              name: "用户激活",
              component: UserActive
            }
          ]

        },
        {
          path:"product",
          name:"商品管理",
          component: BlankLayout,
          children:[
            {
              path: "list",
              name: "商品查询",
              component: ProductList
            },
            {
              path: "down",
              name: "商品下架",
              component: ProductDown
            },
            {
              path: "add",
              name: "商品添加",
              component: ProductAdd
            }
          ]

        },
      ]
    },

    {
      path: "/loginOut",
      name: "Login",
      component: Login
    },
    {
      path: "/register",
      name: "Register",
      component: Register
    },

  ]
});
